import React, { createContext, useState } from 'react'

// import: components
import LottoSelectButton from './Depend/LottoSelectButton'
import LottoSelectHeader from './Depend/LottoSelectHeader'
import LottoSelectPlayground from './Depend/LottoSelectPlayground'

export const LottoSelectContext = createContext()

const LottoSelect = () => {
  const [numberCollection, setNumberCollection] = useState([])
  const [showButtonState, setShowButtonState] = useState(false)
  const [trashIconState, setTrashIconState] = useState(false)

  const objContext = {
    numberCollection,
    setNumberCollection,
    showButtonState,
    setShowButtonState,
    trashIconState,
    setTrashIconState,
  }

  return (
    <LottoSelectContext.Provider value={objContext}>
      <div className="lotto-select">
        <div className="lotto-select__container">
          <div className="lotto-select__header">
            <LottoSelectHeader />
          </div>
          <div className="lotto-select__playground">
            <LottoSelectPlayground />
          </div>
          <div className="lotto-select__button">
            <LottoSelectButton />
          </div>
        </div>
      </div>
    </LottoSelectContext.Provider>
  )
}

export default LottoSelect
