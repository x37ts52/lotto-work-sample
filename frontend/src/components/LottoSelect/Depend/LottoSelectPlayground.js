import React, { useContext } from 'react'
import { LottoSelectContext } from '../LottoSelect'

const LottoSelectPayground = () => {
  const objContext = useContext(LottoSelectContext)

  const numberRange = []
  const numberCollection = objContext.numberCollection

  // loop: build a range from 1 to 49
  for (let i = 1; i <= 49; i++) {
    numberRange.push(i)
  }

  const selectNumberFromClick = (selectedNumber, index) => {
    const idNumberSquare = document.querySelectorAll('#number-square')

    // check if selectedNumber is in array avaible
    const isNumberAvaible = numberCollection.includes(selectedNumber)

    // statement: if a number is not avaible in array than push number into array and highlight number square in gui
    // or delete similar number from array and disable highlight number square in gui
    if (!isNumberAvaible) {
      // statement: only push selectedNumber into array if array size is under 6
      if (numberCollection.length < 6) {
        idNumberSquare[index].classList.add('lotto-select-playground__number-square--active')
        numberCollection.push(selectedNumber)
      }
    } else {
      idNumberSquare[index].classList.remove('lotto-select-playground__number-square--active')
      // function: delete specified selectedNumber
      numberCollection.splice(numberCollection.indexOf(selectedNumber), 1)
    }

    // statement: if none number square is selected than fade trash icon
    // or full color icon trash
    if (0 === numberCollection.length) {
      objContext.setTrashIconState(false)
    } else {
      objContext.setTrashIconState(true)
    }

    // statement: if array reach size of 6 than show button and set numberCollection in state (because rerender)
    // or hide button
    if (numberCollection.length === 6) {
      objContext.setNumberCollection(numberCollection)
      objContext.setShowButtonState(true)
    } else {
      objContext.setShowButtonState(false)
    }
  }

  return (
    <div className="lotto-select-playground">
      {/* loop: create 49 number squares */}
      {numberRange.map((item, key) => (
        <div key={key} id="number-square" className="lotto-select-playground__number-square" onClick={() => selectNumberFromClick(item, key)}>
          <span className="lotto-select-playground__text">{item}</span>
        </div>
      ))}
    </div>
  )
}

export default LottoSelectPayground
