import React, { Fragment, useContext, useEffect } from 'react'
import { LottoSelectContext } from '../LottoSelect'
import { language } from '../../Common/helper'

const LottoSelectHeader = () => {
  const objContext = useContext(LottoSelectContext)

  const changeTrashIconColor = () => {
    const idTrashIcon = document.querySelector('#trash-icon')

    // statement: change icon color
    if (objContext.trashIconState) {
      idTrashIcon.classList.add('lotto-select-header__icon--active')
    } else {
      idTrashIcon.classList.remove('lotto-select-header__icon--active')
    }
  }

  useEffect(() => {
    changeTrashIconColor()
  })

  const removeAllSelectedNumbers = () => {
    const idNumberSquare = document.querySelectorAll('#number-square')

    // statement: if trash icon is active than remove all selectedNumbers
    if (objContext.trashIconState) {
      // loop: remove all activated trash icons
      idNumberSquare.forEach((item, index) => {
        item.classList.remove('lotto-select-playground__number-square--active')
      })

      // set all useStates to default
      objContext.setNumberCollection([])
      objContext.setShowButtonState(false)
      objContext.setTrashIconState(false)
    }
  }

  return (
    <Fragment>
      <div className="lotto-select-header__left">
        <span className="lotto-select-header__title">{language('TITLE', 'PLAYGROUND_TITLE')}</span>
      </div>
      <div className="lotto-select-header__right">
        <i id="trash-icon" className="fas fa-trash-alt lotto-select-header__icon" onClick={() => removeAllSelectedNumbers()}></i>
      </div>
    </Fragment>
  )
}

export default LottoSelectHeader
