import React, { Fragment, useContext } from 'react'
import { LottoSelectContext } from '../LottoSelect'
import { language } from '../../Common/helper'

const LottoSelectButton = () => {
  const objContext = useContext(LottoSelectContext)

  const apiSendNumbers = (numberCollection) => {
    // function: sort an array in ascending order
    numberCollection.sort((a, z) => {
      return a - z
    })

    // alert is only for testing!
    alert(numberCollection)

    // INFO
    // implement api connection
  }

  return (
    <Fragment>
      {objContext.showButtonState ? (
        <button type="button" className="button" onClick={() => apiSendNumbers(objContext.numberCollection)}>
          {language('BUTTON', 'NEXT')}
        </button>
      ) : null}
    </Fragment>
  )
}

export default LottoSelectButton
