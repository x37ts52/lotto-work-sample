import jsonLanguage from './language.json'

const language = (translateElement, translateText) => {
  // detect language from browser
  const browserLanguage = navigator.language || navigator.userLanguage

  // output follow the JSON structure
  return jsonLanguage[translateElement][translateText][browserLanguage]
}

export default language
