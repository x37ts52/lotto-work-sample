import React from 'react'
import './App.css'

import LottoSelect from './components/LottoSelect/LottoSelect'

const App = () => {
  return <LottoSelect />
}

export default App
